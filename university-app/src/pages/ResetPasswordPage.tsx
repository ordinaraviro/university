import React from'react';
import ResetPasswordForm from '../components/forms/ResetPasswordForm/ResetPasswordForm';

const ResetPasswordPage: React.FC = () => {
  return (
    <div className="form-container">
      <ResetPasswordForm />
    </div>
  );
};

export default ResetPasswordPage;
