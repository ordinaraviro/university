import React from'react';
import LoginForm from '../components/forms/LoginForm/LoginForm';

const LoginPage: React.FC = () => {
  return (
    <div className="form-container">
      <LoginForm />
    </div>
  );
};

export default LoginPage;
