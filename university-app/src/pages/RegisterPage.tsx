import React from'react';
import RegisterForm from '../components/forms/RegisterForm/RegisterForm';

const RegisterPage: React.FC = () => {
  return (
    <div className="form-container">
      <RegisterForm />
    </div>
  );
};

export default RegisterPage;
