import React from'react';
import ForgotPasswordForm from '../components/forms/ForgotPasswordForm/ForgotPasswordForm';

const ResetPasswordPage: React.FC = () => {
  return (
    <div className="form-container">
      <ForgotPasswordForm />
    </div>
  );
};

export default ResetPasswordPage;
