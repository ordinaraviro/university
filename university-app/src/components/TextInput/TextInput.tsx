import React, { ChangeEvent } from 'react';

interface TextInputProps {
  label: string;
  type: string;
  value: string;
  placeholder: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const TextInput: React.FC<TextInputProps> = ({ label, type, value,placeholder, onChange }) => {
  return (
    <div className='input-comtainer'>
      <label className='input-label'>{label}</label>
      <input type={type} value={value} onChange={onChange} placeholder={placeholder} className='input-field'/>
    </div>
  )
}

export default TextInput;
