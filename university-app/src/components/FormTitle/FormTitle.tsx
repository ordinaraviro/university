import React from 'react';

interface FormTitleProps {
  title: string;
}

const FormTitle: React.FC<FormTitleProps> = ({ title }) => {
  return <h2 className="form-title">{title}</h2>;
};

export default FormTitle;
