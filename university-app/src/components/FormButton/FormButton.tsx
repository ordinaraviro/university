import React, { FormEvent } from "react";

interface ButtonProps {
  onClick: (event: FormEvent) => void;
  label: string;
}

const Button: React.FC<ButtonProps> = ({ onClick, label }) => {
  return (
    <button onClick={onClick} className="form-button">
      {label}
    </button>
  );
}

export default Button;
