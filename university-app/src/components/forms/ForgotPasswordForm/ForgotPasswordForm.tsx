import React, { ChangeEvent, FormEvent, useState } from'react';
import TextInput from '../../TextInput/TextInput';
import Button from '../../FormButton/FormButton';
import { ReactComponent as FormLogo } from '../../../assets/icons/logo-form.svg';
import FormTitle from '../../FormTitle/FormTitle';
import FormDescription from '../../FormDescription/FormDescription';
import FormLink from '../../FormLink/FormLink';

const ForgotPasswordForm: React.FC = () => {
  const [email, setEmail] = useState('');

  const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    alert(`Email: ${email}`); // TODO logic
  };

  return (
    <>
      <div className="logo-container forgot-password-logo">
        <FormLogo className='logo-icon'/>
      </div>
      <form onSubmit={handleSubmit} className='forgot-password-form'>
        <FormTitle title="Reset Password"/>
        <FormDescription
          content="Don't worry, happens to the best of us. Enter the email address associated with your account and we'll send you a link to reset."
        />
        <TextInput
          label="Email"
          type="email"
          value={email}
          placeholder='name@mail.com'
          onChange={handleUsernameChange}
        />
        <Button label="Reset" onClick={handleSubmit}/>
        <FormLink label="Cancel" path=""/>
      </form>
    </>
  );
};

export default ForgotPasswordForm;
