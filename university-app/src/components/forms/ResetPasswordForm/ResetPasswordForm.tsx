import React, { ChangeEvent, FormEvent, useState } from'react';
import TextInput from '../../TextInput/TextInput';
import Button from '../../FormButton/FormButton';
import { ReactComponent as FormLogo } from '../../../assets/icons/logo-form.svg';
import FormTitle from '../../FormTitle/FormTitle';
import Checkbox from '../../Checkbox/Checkbox';
import FormDescription from '../../FormDescription/FormDescription';
import FormLink from '../../FormLink/FormLink';

const LoginForm: React.FC = () => {
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [submitted, setSubmitted] = useState(false);

  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleRepeatPasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setRepeatPassword(event.target.value);
  };

  const handleShowPasswordChange = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    alert(`Password: ${password}`); // TODO logic
    setSubmitted(true);
  };

  return (
    <>
      <div className="logo-container">
        <FormLogo className='logo-icon'/>
      </div>
      {submitted ? (
        <form onSubmit={handleSubmit} className='reset-password-form'>
        <FormTitle title="Password Changed"/>
        <FormDescription
          content="You can use your new password to log into your account"
        />
        <Button label="Log In" onClick={handleSubmit}/>
        <FormLink label="Go to Home" path=""/>
      </form>
      ) : (
        <form onSubmit={handleSubmit}>
          <FormTitle title="Reset Your Password"/>
          <TextInput
            label="New Password"
            type={showPassword ? 'text' : 'password'}
            value={password}
            placeholder=''
            onChange={handlePasswordChange}
          />
          <TextInput
            label="Confirm Password"
            type={showPassword ? 'text' : 'password'}
            value={repeatPassword}
            placeholder=''
            onChange={handleRepeatPasswordChange}
          />
          <Checkbox
            label="Show Password"
            checked={showPassword}
            onChange={handleShowPasswordChange}
          />
          <Button label="Reset" onClick={handleSubmit}/>
        </form>
      )}
    </>
  );
};

export default LoginForm;
