import React, { ChangeEvent, FormEvent, useState } from'react';
import TextInput from '../../TextInput/TextInput';
import Button from '../../FormButton/FormButton';
import { ReactComponent as FormLogo } from '../../../assets/icons/logo-form.svg';
import FormTitle from '../../FormTitle/FormTitle';
import Checkbox from '../../Checkbox/Checkbox';

const LoginForm: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const handleUsernameChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleShowPasswordChange = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    alert(`Email: ${email}, password: ${password}`); // TODO logic
  };

  return (
    <>
      <div className="logo-container">
        <FormLogo className='logo-icon'/>
      </div>
      <form onSubmit={handleSubmit} className='login-form'>
        <FormTitle title="Welcome!"/>
        <TextInput
          label="Email"
          type="email"
          value={email}
          placeholder=''
          onChange={handleUsernameChange}
        />
        <TextInput
          label="Password"
          type={showPassword ? 'text' : 'password'}
          value={password}
          placeholder=''
          onChange={handlePasswordChange}
        />
        <Checkbox
          label="Show Password"
          checked={showPassword}
          onChange={handleShowPasswordChange}
        />
        <Button label="Login" onClick={handleSubmit}/>
      </form>
    </>
  );
};

export default LoginForm;
