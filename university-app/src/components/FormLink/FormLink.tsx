import React from "react";
import { Link } from "react-router-dom";

interface LinkProps {
  path: string
  label: string;
}

const FormLink: React.FC<LinkProps> = ({ path, label }) => {
  return (
    <Link to={path} className="form-link">
      {label}
    </Link>
  );
}

export default FormLink;
