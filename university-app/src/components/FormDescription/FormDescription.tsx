import React from 'react';

interface FormDescriptionProps {
  content: string;
}

const FormDescription: React.FC<FormDescriptionProps> = ({ content }) => {
  return (
    <div className='form-description'>
      {content}
    </div>
  )
}

export default FormDescription;
